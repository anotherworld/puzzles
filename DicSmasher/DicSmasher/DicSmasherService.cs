﻿using System.Collections.Generic;

namespace DicSmasher
{
    public class DicSmasherService : IDicSmasherService<string, string>
    {
        private readonly PairList<string, IDictionary<string, string>> _dictionaryDescriptors;

        public DicSmasherService()
        {
            _dictionaryDescriptors = new PairList<string, IDictionary<string, string>>();
        }

        public void AddDictionary(IDictionary<string, string> dictionary, string name)
        {
            _dictionaryDescriptors.Add(name, dictionary);
        }

        public IDictionary<string, KeyValuePair<string, string>> Smash()
        {
            var result = new Dictionary<string, KeyValuePair<string, string>>();
            foreach (var descriptor in _dictionaryDescriptors)
            {
                var dictionary = descriptor.Value;
                foreach (var key in dictionary.Keys)
                    result[key] = new KeyValuePair<string, string>(dictionary[key], descriptor.Key);
            }
            return result;
        }
    }
}