﻿using System.Collections.Generic;
using NUnit.Framework;

namespace DicSmasher
{
    public class DicSmasherTests
    {
        [Test]
        public void ShouldSmashTwoCorrectly()
        {
            // Arrange
            var dicA = new Dictionary<string, string>
            {
                { "A", "1" },
                { "B", "2" },
                { "C", "3" }
            };

            var dicB = new Dictionary<string, string>
            {
                { "C", "5" },
                { "D", "6" },
                { "E", "7" }
            };

            // Act
            var smasher = new DicSmasherService();
            smasher.AddDictionary(dicA, "Apples");
            smasher.AddDictionary(dicB, "Oranges");
            var actual = smasher.Smash();

            // Assert
            Assert.AreEqual(5, actual.Count);
            Assert.AreEqual(new KeyValuePair<string, string>("1", "Apples"), actual["A"]);
            Assert.AreEqual(new KeyValuePair<string, string>("5", "Oranges"), actual["C"]);
            Assert.AreEqual(new KeyValuePair<string, string>("7", "Oranges"), actual["E"]);
        }

        [Test]
        public void ShouldSmashThreeCorrectly()
        {
            // Arrange
            var dicA = new Dictionary<string, string>
            {
                { "A", "1" },
                { "B", "2" },
                { "C", "3" }
            };

            var dicB = new Dictionary<string, string>
            {
                { "C", "5" },
                { "D", "6" },
                { "E", "7" }
            };

            var dicC = new Dictionary<string, string>
            {
                { "A", "9" },
                { "F", "10" },
                { "G", "11" },
                { "H", "12" },
                { "I", "13" }
            };

            // Act
            var smasher = new DicSmasherService();
            smasher.AddDictionary(dicA, "Apples");
            smasher.AddDictionary(dicB, "Oranges");
            smasher.AddDictionary(dicC, "Bananas");
            var actual = smasher.Smash();

            // Assert
            Assert.AreEqual(9, actual.Count);
            Assert.AreEqual(new KeyValuePair<string, string>("9", "Bananas"), actual["A"]);
            Assert.AreEqual(new KeyValuePair<string, string>("5", "Oranges"), actual["C"]);
            Assert.AreEqual(new KeyValuePair<string, string>("7", "Oranges"), actual["E"]);
            Assert.AreEqual(new KeyValuePair<string, string>("13", "Bananas"), actual["I"]);
        }
    }
}
