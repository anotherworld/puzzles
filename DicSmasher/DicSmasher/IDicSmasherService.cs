﻿using System.Collections.Generic;

namespace DicSmasher
{
    public interface IDicSmasherService<TKey, TValue>
    {
        void AddDictionary(IDictionary<TKey, TValue> dictionary, string name);
        IDictionary<TKey, KeyValuePair<TValue, string>> Smash();
    }
}